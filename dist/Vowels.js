"use strict";
function Vowels(word) {
    var count = 0;
    for (var i = 0; i < word.length; i++) {
        if (word.charAt(i) == 'A' || word.charAt(i) == 'a' ||
            word.charAt(i) == 'E' || word.charAt(i) == 'e' ||
            word.charAt(i) == 'I' || word.charAt(i) == 'i' ||
            word.charAt(i) == 'O' || word.charAt(i) == 'o' ||
            word.charAt(i) == 'U' || word.charAt(i) == 'u') {
            count++;
        }
    }
    return count;
}
console.log(Vowels('Thanakrit'));
